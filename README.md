# fmpi_sync

Library providing a dummy subroutine to be called to prevent an optimizing Fortran compiler to move synchronization calls outside of the sync region.

# Build

Set your desired Fortran compiler, e.g. `export FC=ifort`, and run make.

