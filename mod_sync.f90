module fmpi_sync
  use, intrinsic  ::  iso_c_binding

  interface force_sync
    module procedure force_sync_single_real   
    module procedure force_sync_single_complex 
    module procedure force_sync_double_real   
    module procedure force_sync_double_complex 
  end interface force_sync

  contains
    subroutine force_sync_single_real(in_array)

      implicit none
      real(kind=C_FLOAT), intent(inout)  ::  in_array(:)
      
    end subroutine force_sync_single_real

    subroutine force_sync_double_real(in_array)

      implicit none
      real(kind=C_DOUBLE), intent(inout)  ::  in_array(:)
      
    end subroutine force_sync_double_real

    subroutine force_sync_single_complex(in_array)

      implicit none
      complex(kind=C_FLOAT_COMPLEX), intent(inout)  ::  in_array(:)
      
    end subroutine force_sync_single_complex

    subroutine force_sync_double_complex(in_array)

      implicit none
      complex(kind=C_DOUBLE_COMPLEX), intent(inout)  ::  in_array(:)
      
    end subroutine force_sync_double_complex
end module fmpi_sync
